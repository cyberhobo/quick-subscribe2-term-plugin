<?php
/*
 * Template variables in scope:
 * WP_User $user The new user
 * string $password The new user plaintext password
 * object $term The term subscribed to
 */
?>
You have successfully subscribed to <?php echo stripslashes( $term->name ); ?>.

You may log in to manage your subscriptions:
Username: <?php echo stripslashes( $user->user_login ); ?>

Password: <?php echo $password; ?>

Login Page: <?php echo wp_login_url(); ?>

