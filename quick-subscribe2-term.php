<?php
/*
  Plugin Name: Quick Subscribe2 Term
  Description: Subscribe an email address to a term with minimum hassle.
  Version: 0.2
  License: GPL2+
  Author: Dylan Kuhn
  Author URI: http://www.cyberhobo.net/
  Minimum WordPress Version Required: 3.0
 */

/*
  Copyright (c) 2012 Dylan Kuhn

  This program is free software; you can redistribute it
  and/or modify it under the terms of the GNU General Public
  License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any
  later version.

  This program is distributed in the hope that it will be
  useful, but WITHOUT ANY WARRANTY; without even the implied
  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE. See the GNU General Public License for more
  details.
 */

QuickSubscribe2Term::load();

class QuickSubscribe2Term {
	const VERSION = '0.2';

	static $dir_path;
	static $url_path;
	static $basename;

	static function load() {
		self::$dir_path = dirname( __FILE__ );
		self::$basename = plugin_basename( __FILE__ );
		self::$url_path = plugins_url( '', __FILE__ );
		load_plugin_textdomain( 'QuickSubscribe2Term', '', path_join( dirname( self::$basename ), 'lang' ) );

		add_action( 'init', array( __CLASS__, 'action_init' ) );
		add_action( 'widgets_init', array( __CLASS__, 'action_widgets_init' ) );
		add_action( 'admin_notices', array( __CLASS__, 'action_admin_notices' ) );
		add_action( 'wp_ajax_qs2t_subscribe', array( __CLASS__, 'action_wp_ajax_qs2t_subscribe' ) );
		add_action( 'wp_ajax_nopriv_qs2t_subscribe', array( __CLASS__, 'action_wp_ajax_qs2t_subscribe' ) );
	}

	/**
	 * Get the users subscribed to a term.
	 *
	 * @param int|string $term The term ID or slug.
	 * @param string $taxonomy The term taxonomy.
	 * @return array An array of WP_User objects.
	 */
	static public function get_subscribed_users( $term, $taxonomy = 'category' ) {
		global $mysubscribe2;

		$subscribed_users = array();

		if ( is_int( $term ) ) 
			$term = get_term( $term, $taxonomy );
		else if ( is_string( $term ) )
			$term = get_term_by( 'slug', $term, $taxonomy );

		if ( !empty( $term ) ) {
			$subscribed_users = get_users( array( 
				'meta_key' => $mysubscribe2->get_usermeta_keyname( 's2_cat' ) . $term->term_id,
				'meta_value' => $term->term_id,
				'meta_compare' => '=',
			) );
		}

		return $subscribed_users;
	}

	static function action_wp_ajax_qs2t_subscribe() {
		global $mysubscribe2;
		
		// Javascript nonce check is our spam defense
		if ( !wp_verify_nonce( $_REQUEST['subscribe_nonce'], 'qs2t_subscribe' ) )
			die( -1 );

		$user = wp_get_current_user();

		$the_term = get_term_by( 'name', $_REQUEST['subscribe_term'], $_REQUEST['subscribe_taxonomy'] );

		$success = '';

		if ( empty( $user->ID ) and isset( $_REQUEST['subscribe_email'] ) ) {
			
			$email = is_email( $_REQUEST['subscribe_email'] );

			if ( !$email ) 
				die( '<div class="error">' . __( 'Sorry, that email address is not valid.', 'QuickSubscribe2Term' ) . '</div>' );
				
			$user = get_user_by( 'email', $email );

			if ( empty( $user->ID ) ) {
			
				$password = wp_generate_password();

				$user_id = wp_create_user( $email, $password, $email );

				if ( is_wp_error( $user_id ) ) 
					die( $user_id->get_error_message() );

				$success = __( 'You have been subscribed. You\'ll receive an email with a username and password which you can use to manage your subscriptions.', 'QuickSubscribe2Term' ) . ' ';

				$user = get_user_by( 'id', $user_id );

				self::new_user_notification( $user, $password, $the_term );
			}
		}

		if ( !$the_term )
			die( '<div class="error">' . sprintf( __( 'Sorry, %s was not found.', 'QuickSubscribe2Term' ), $_REQUEST['subscribe_term'] ) .  '</div>' );

		$subscribed_term_ids = explode( ',', get_user_meta( $user->ID, $mysubscribe2->get_usermeta_keyname( 's2_subscribed' ), true ) );

		if ( in_array( $the_term->term_id, $subscribed_term_ids ) ) {

			// Unsubscribe
			$subscribed_term_ids = array_diff( $subscribed_term_ids, array( $the_term->term_id ) );
			delete_user_meta( $user->ID, $mysubscribe2->get_usermeta_keyname( 's2_cat' ) . $the_term->term_id );
			$success .= __( 'Unsubscribed.', 'QuickSubscribe2Term' );

		} else {

			// Ensure the required sub2 metadata exists
			add_user_meta( $user->ID, $mysubscribe2->get_usermeta_keyname( 's2_format' ), 'excerpt', true );
			add_user_meta( $user->ID, $mysubscribe2->get_usermeta_keyname( 's2_authors' ), '', true );

			// Subscribe
			$subscribed_term_ids[] = $the_term->term_id;
			update_user_meta( $user->ID, $mysubscribe2->get_usermeta_keyname( 's2_cat' ) . $the_term->term_id, $the_term->term_id );
			
			// Don't repeat ourselves
			if ( empty( $success ) )
				$success .= __( 'You have successfully subscribed.', 'QuickSubscribe2Term' );

		}

		if ( update_user_meta( $user->ID, $mysubscribe2->get_usermeta_keyname( 's2_subscribed' ), trim( implode( ',', $subscribed_term_ids ), ', ' ) ) )
			echo '<div class="message">' . $success . '</div>';
		else 
			echo '<div class="error">' . __( 'Sorry, there was a problem updating your subscription.', 'QuickSubscribe2Term' ) . '</div>';
		
		exit();
	}

	static function new_user_notification( $user, $password, $term ) {

		// The blogname option is escaped with esc_html on the way into the database in sanitize_option
		// we want to reverse this for the plain text arena of emails.
		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

		$subject = apply_filters( 'qs2t_new_user_email_subject', sprintf( __( '[%s] Your username and password', 'QuickSubscribe2Term' ), $blogname ) );

		$template = locate_template( 'qs2t-new-user-email.php' );
		if ( !$template ) 
			$template = path_join( self::dir_path, 'new-user-email.php' );

		ob_start();
		require( $template );
		$message = ob_get_clean();

		wp_mail( stripslashes( $user->user_email ), $subject, $message );
	}

	static function action_init() {
	}

	/**
	 * Register the subscribe widget.
	 */
	public function action_widgets_init() {
		register_widget( 'QuickSubscribe2TermWidget' );
	}

	/**
	 * Raise a notice and deactivate if dependencies are missing.
	 */
	public function action_admin_notices() {
		if ( !class_exists( 's2class' ) ) {
			echo '<div class="updated fade">';
			echo __( 'The Subscribe2 plugin must be installed and activated before Quick Subscribe2 Term.', 'QuickSubscribe2Term' );
			echo '</div>';
			deactivate_plugins( self::$basename );
		}
	}
} // end QuickSubscribe2Term class

class QuickSubscribe2TermWidget extends WP_Widget {

	// Construct Widget
	function __construct() {
		$default_options = array(
			'description' => __( 'Subscribe a user or email address to a term with minimal fuss.', 'QuickSubscribe2Term' )
		);
		parent::__construct( false, __( 'Quick Subscribe2 Term', 'QuickSubscribe2Term' ), $default_options );
	}

	// Display Widget
	function widget( $args, $instance ) {
		global $mysubscribe2;

		wp_enqueue_script( 'qs2t-subscribe-form', path_join( QuickSubscribe2Term::$url_path, 'subscribe-form.js' ), array( 'jquery' ), QuickSubscribe2Term::VERSION, true );
		wp_localize_script( 'qs2t-subscribe-form', 'qs2t_subscribe_form_env', array( 
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'spinner_url' => path_join( QuickSubscribe2Term::$url_path, 'media/ajax-loader.gif' ),
			'nonce' => wp_create_nonce( 'qs2t_subscribe' ),
			'ajax_error_message' => __( 'Sorry, there was a problem reaching the server', 'QuickSubscribe2Term' ),
		) );

		extract( $args );

		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget . $before_title . $title . $after_title;

		// Template variables
		$widget = $this;

		$template = locate_template( 'qs2t-subscribe-form.php' );
		if ( !$template )
			$template = path_join( QuickSubscribe2Term::$dir_path, 'subscribe-form.php' );

		require( $template );

		echo $after_widget;
	}

	// Update Widget
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['subscribe_taxonomy'] = sanitize_text_field( $new_instance['subscribe_taxonomy'] );
		$instance['subscribe_term'] = sanitize_text_field( $new_instance['subscribe_term'] );

		return $instance;
	}

	// Default value logic
	function get_default_value( $instance, $field, $fallback = '', $escape_callback = 'esc_attr' ) {
		if ( isset( $instance[$field] ) )
			$value = $instance[$field];
		else
			$value = $fallback;

		if ( function_exists( $escape_callback ) )
			$value = call_user_func( $escape_callback, $value );

		return $value;
	}

	// Display Widget Control
	function form( $instance ) {
		$taxonomies = get_taxonomies();
?>
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"
		 title="<?php _e( 'Widget heading, leave blank to omit.', 'QuickSubscribe2Term' ); ?>">
		 <?php _e( 'Title:', 'QuickSubscribe2Term' ); ?>
		<span class="help-tip">?</span>
		<input class="widefat"
			 id="<?php echo $this->get_field_id( 'title' ); ?>"
			 name="<?php echo $this->get_field_name( 'title' ); ?>"
			 type="text"
			 value="<?php echo $this->get_default_value( $instance, 'title' ); ?>" />
	</label>
</p>
<p>
	<label for="<?php echo $this->get_field_id( 'subscribe_term' ); ?>"
			 title="<?php _e( 'The term to subscribe to.', 'QuickSubscribe2Term' ); ?>">
		<?php _e( 'Subscribe term:', 'QuickSubscribe2Term' ); ?>
		<span class="help-tip">?</span>
		<input class="widefat"
				 id="<?php echo $this->get_field_id( 'subscribe_term' ); ?>"
				 name="<?php echo $this->get_field_name( 'subscribe_term' ); ?>"
				 type="text"
				 value="<?php echo $this->get_default_value( $instance, 'subscribe_term' ); ?>" />
	</label>
</p>
<p>
	<label for="<?php echo $this->get_field_id( 'subscribe_taxonomy' ); ?>"
			 title="<?php _e( 'The taxonomy that contains the subscribe term.', 'QuickSubscribe2Term' ); ?>">
		<?php _e( 'Subscribe Term taxonomy:', 'QuickSubscribe2Term' ); ?>
		<span class="help-tip">?</span>
		<select id="<?php echo $this->get_field_id( 'subscribe_taxonomy' ); ?>" name="<?php echo $this->get_field_name( 'subscribe_taxonomy' ); ?>">
		<?php foreach( $taxonomies as $taxonomy ) : ?>
			<option<?php echo $taxonomy == $this->get_default_value( $instance, 'subscribe_taxonomy' ) ? ' selected="selected"' : ''; ?>>
				<?php echo $taxonomy; ?>
			</option>
		<?php endforeach; ?>
		</select>
	</label>
</p>
<?php
	 }

 }
