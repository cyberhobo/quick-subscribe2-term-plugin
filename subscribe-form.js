/**global jQuery, qs2t_subscribe_form_env */
jQuery( function( $ ) {
	var $form = $( 'form.quick-subscribe2-term' ),
		$nonce_input = $form.find( 'input[name=subscribe_nonce]' ),
		$email_input = $form.find( 'input[name=subscribe_email]' ),
		email_prompt_cleared = false;

	$nonce_input.val( qs2t_subscribe_form_env.nonce );

	$email_input.focus( function() {
		if ( !email_prompt_cleared ) {
			$email_input.val( '' );
			email_prompt_cleared = true;
		}
	} );

	$form.submit( function() {
		$form.prepend( $( '<div class="loading_indicator"><img src="' + qs2t_subscribe_form_env.spinner_url + '" alt="Loading..." /></div>' ) );
		$.get( qs2t_subscribe_form_env.ajaxurl, $form.serialize(), function( message ) {

			$form.html( message );

		} ).error( function() {

			$form.html('<div class="error">' + qs2t_subscribe_form_env.ajax_error_message + '</div>');

		} );
		return false;
	} );
} ); 
