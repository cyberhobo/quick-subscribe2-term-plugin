<?php
/**
 * Variables in scope:
 * QuickSubscribe2TermWidget $widget     The widget generating this form
 * array       $instance   The widget instance data
 */

$user = wp_get_current_user(); 
$subscribed_terms = empty( $user->ID ) ? array() : explode( ',', get_user_meta( $user->ID, $mysubscribe2->get_usermeta_keyname( 's2_subscribed' ), true ) );
$the_term = get_term_by( 'name', $instance['subscribe_term'], $instance['subscribe_taxonomy'] );
$action_label = in_array( $the_term->term_id, $subscribed_terms ) ? 'unsubscribe' : 'subscribe';
?>

<form class="quick-subscribe2-term" method="post">
	
	<input id="<?php echo $widget->id; ?>-nonce" name="subscribe_nonce" type="hidden" />

	<input id="<?php echo $widget->id; ?>-action" name="action" type="hidden" value="qs2t_subscribe" />

	<input id="<?php echo $widget->id; ?>-taxonomy" name="subscribe_taxonomy" type="hidden" value="<?php echo $instance['subscribe_taxonomy']; ?>" />

	<input id="<?php echo $widget->id; ?>-term" name="subscribe_term" type="hidden" value="<?php echo $instance['subscribe_term']; ?>" />

	<?php if ( empty( $user->ID ) ) : ?>

	<input id="<?php echo $widget->id; ?>-email" name="subscribe_email" type="text" value="enter email address" />
	
	<?php endif; ?>

	<input id="<?php echo $widget->id; ?>-submit" name="subscribe_submit" class="submit" type="submit" value="<?php echo $action_label; ?>" />

</form>
